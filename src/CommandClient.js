const { Client } = require("discord.js");
const CommandManager = require("./CommandManager");
const Context = require("./Context");
const guild = require("./Guild");
const path = require("path");


class CommandClient extends Client {
    constructor(opt) {
        super(opt);
        
        this.autoClearUnknownButtons = opt.autoClearUnknownButtons || false;
        this.defaultPrefix = opt.defaultPrefix || "::";
        this.commandsPath = path.resolve(process.cwd(), opt.commandsPath || "./commands");

        this.manager = new CommandManager(this);

        this.__messageHandler = null;
        this.__commandPreHandler = null;

        this.on("messageCreate", this.#messageHandler);
        this.on("messageUpdate", this.#messageHandler);
        this.on("interactionCreate", this.#interactionHandler);

        this.cache = {
            components: {},
            cooldowns: {}
        };

    }

    async #commandHandler(ctx) {

        if (!ctx.command) return;

        if (this.__commandPreHandler) await this.__commandPreHandler(ctx); 
        
        try {
            await ctx.command.run(ctx);
        } catch (error) {
            ctx.error = error;
            this.emit("commandError", ctx);
        }

        this.emit("endCommandHandler", ctx);
    }

    async #messageHandler(oldMsg, newMsg) {
        let message = newMsg || oldMsg;

        guild(message.guild);

        if (!message.channel.isTextBased()) return;

        const ctx = new Context(message);

        if (this.__messageHandler) await this.__messageHandler(ctx);
        
        await ctx.init();
        
        if (new RegExp(`^${ctx.guild.prefix}[^\\s+]`).test(ctx.content)) this.#commandHandler(ctx);
    }
    
    async #interactionHandler(interaction) {
        if (interaction.isButton()) {
            if (this.cache.components[interaction.customId]?.fn) this.cache.components[interaction.customId].fn(interaction);
            else if (this.autoClearUnknownButtons)
                interaction.update({ components: [] });
        }
        if (interaction.isSelectMenu()) {
            if (this.cache.components[interaction.customId]?.fn) this.cache.components[interaction.customId].fn(interaction);
        }
    }

    async commandPreHandler(fn) {
        if (typeof fn === "function") this.__commandPreHandler = fn;
    }

    async messageHandler(fn) {
        if (typeof fn === "function") this.__messageHandler = fn;
    }

    initCommands(path) {
        this.manager.load(path || this.commandsPath);
    }
}

module.exports = CommandClient;