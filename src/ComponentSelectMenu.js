module.exports = class ComponentSelectMenu {
    constructor(client, selectMenu) {
        this.client = client;
        this.selectMenu = selectMenu;
    }

    addOption(label, value, description) {
        let opt = {};
        if (typeof label === 'string') {
            opt = { label, description, value };
        } else opt = label;

        this.selectMenu.addOptions(opt);
        return this;
    }
}