const { Collection } = require("discord.js");
const { RequireArgError, ArgTypeError } = require("./Errors");

class Command {
    constructor(manager, prop) {
        this.manager = manager;
        this.prop = {
            ...prop.config,
            ...prop,
        };

        this.permissions        = this.prop.permissions || "All";
        this.aliases            = this.prop.aliases     || [];
        this.ignored            = this.prop.ignore      || false;
        this.category           = this.prop.category    || "other";
        this.cooldown           = this.prop.cooldown    || 0;
        this.requiredArgs       = this.prop.args;
        this.description        = this.prop.description;
        this.name               = this.prop.name;
        this.path               = this.prop.path;
        this._run               = this.prop.run;

        this.parent             = this.prop.parent || null;
        
        this.subcommands        = new Collection();
        
        this.args               = [];
        
        if (this.prop.init) {
            this.prop.init(this);
            this.prop.init = null;
        }

        for (const alias of this.aliases) {
            if (this.parent) this.parent.subcommands.set(alias, this.name);
            else this.manager.commands.set(alias, this.name);
        }
        
        let call = []; 
        let step = (cmd) => cmd.parent ? (call.push(cmd.name), step(cmd.parent)) : call.push(cmd.name);
        step(this);
        this.callName = call.reverse();

        this.#parseArgs();

    }


    #parseArgs() {
        if (this.requiredArgs) {
            let re_notRequire = /^\?|\?$/;
            let re_arg = /^\??(\w+)-(\w+)\??$/;
            let endNotRequire = false;
            for (let arg of this.requiredArgs) {
                if (re_notRequire.test(arg)) endNotRequire = true;
                if (!re_notRequire.test(arg) && endNotRequire) throw new Error("Mandatory arguments cannot come after optional arguments");

                let res = re_arg.exec(arg);
                if (!res) throw new Error(`[Command: ${this.callName}] The argument template does not match. Template: "Phrase-Type", Your agrgument: "${arg}"`);
                if (!res[1]) throw new Error(`[Command: ${this.callName}] The phrase of the argument is a required parameter. Template: "Phrase-Type", Your agrgument: "${arg}"`);
                
                let type = res[2].toLowerCase();

                if (['int', 'num', 'number'].includes(type)) type = 'integer';
                if (['str'].includes(type)) type = 'string';
                if (['bool'].includes(type)) type = 'boolean';
                
                this.args.push({
                    phrase: res[1].toLowerCase(),
                    require: !re_notRequire.test(arg),
                    type
                });
            }
        }
    }

    /***
     * @param {String|undefined} subcommand Subcommand name or alias
     */
    #getSubcommand(subcommand) {
        let aliasOrSubcommand = this.subcommands.get(subcommand?.toLowerCase());
        if (!aliasOrSubcommand) return;
        if (aliasOrSubcommand instanceof Command) return aliasOrSubcommand;
        return this.subcommands.get(aliasOrSubcommand);
    }

    subcommand(name, options, run) {
        let prop = {
            ...this.prop,
            aliases: null,
            parent: this,
            init: undefined,
            args: []
        };
        
        if (Array.isArray(name)) {
            prop.aliases = name.splice(1);
            name = name[0];
        }

        if (this.subcommands.has(name)) throw new Error(`The subcommand "${name}" for command "${this.callName.join(" ")}" has already been created`);
        if (prop.aliases) for (let alias of prop.aliases) 
            if (this.subcommands.has(alias)) console.warn(`The subcommand alias "${alias}" for command "${this.callName.join(" ")}" has already been created`);
        
        if (typeof options === "object") prop = {...prop, ...options};
        if (typeof options === "function") run = options;

        prop.name = name;
        prop.run = run;

        let subcommand = new Command(this.manager, prop);

        this.subcommands.set(name, subcommand);
        return subcommand;
    }

    subsearch(args) {
        if (!Array.isArray(args)) return;
        let command = this.#getSubcommand(args[0]);
        if (command) {
            if (command.ignored || command.removed) return;   /// ???
            return command.subsearch(args.splice(1));
        }
        else return this;
    }

    compareArgs(ctx) {
        let x = false;
        let indexArgs = ctx.args.filter(v => (v.phrase?x=true:null, !x));
        let comparedArgs = [];

        if (this.args.length === 1 && this.args[0].type === "string") 
                return [ctx.commandArgs.join(' ')];

        for (let i = 0; i < this.args.length; i++) {
            const commandArg = this.args[i];
            let ctxArg = null;

            if (indexArgs[i]) ctxArg = indexArgs[i];
            else ctxArg = ctx.args.find(v => v.phrase === commandArg.phrase);

            if (!ctxArg && this.args.filter(v => v.require).length > i) throw new RequireArgError();

            if (!ctxArg?.values.find(v => v.type === commandArg.type) && ctxArg) throw new ArgTypeError();

            comparedArgs.push(ctxArg?.values.find(v => v.type === commandArg.type).value);
        }
        comparedArgs = comparedArgs.concat(ctx.commandArgs.slice(this.args.length));
        
        return comparedArgs;
    }

    checkPermissions(member) {
        return this.manager.permissions.check(member, this);
    }

    checkCooldown(ctx) {
        if (!this.cooldown) return false;
        if (!ctx.client.cache.cooldowns[ctx.user.id]) ctx.client.cache.cooldowns[ctx.user.id] = {};

        let cmd = this.callName.join(' ');
        let lastCall = ctx.client.cache.cooldowns[ctx.user.id][cmd];
        if (lastCall && lastCall.timestamp + this.cooldown > Date.now()) return true;

        ctx.client.cache.cooldowns[ctx.user.id][cmd] = {
            user: ctx.user,
            command: this,
            timestamp: Date.now() 
        };
        return false;
    }

    async run(ctx) {
        // Checking cooldown
        if (this.checkCooldown(ctx)) {
            this.manager.client.emit("commandCooldownError", ctx);
            return;
        }
        // Checking permissions
        if (!this.checkPermissions(ctx.member)) {
            this.manager.client.emit("commandPermissionsError", ctx);
            return;
        }
        // Comparing args with ctx.commandArgs
        let args = null;
        try {
            args = this.compareArgs(ctx);

            // Executing command
            if (this._run) await this._run(ctx, ...args);
        
        } catch (error) {
            ctx = ctx.create();
            ctx.error = error;
            if (error instanceof RequireArgError) {
                this.manager.client.emit("commandRequireArgError", ctx);
            }
            else if (error instanceof ArgTypeError) {
                this.manager.client.emit("commandArgTypeError", ctx);
            }
            else this.manager.client.emit("commandError", ctx);
            return;
        }
    }

    reload() {
        this.manager.remove(this.callName[0]);
        this.manager.loadOne(this.path);
        return this.manager.get(this.callName);
    }
};

module.exports = Command;