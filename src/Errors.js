module.exports = {
    ArgsError: class extends Error {
        constructor(msg) {
            super(msg);
        }
    },
    RequireArgError: class extends Error {
        constructor(msg) {
            super(msg);
        }
    },
    ArgTypeError: class extends Error {
        constructor(msg) {
            super(msg);
        }
    },
}