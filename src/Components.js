const { ActionRowBuilder, ButtonBuilder, ButtonStyle } = require("discord.js");
// const ComponentSelectMenu = require("./ComponentSelectMenu");


class Components extends ActionRowBuilder {
    constructor(client, ...args) {
        super(...args);
        this.client = client;

        this.filter = i => true;
    }

    get length() {
        return this.components.length;
    }

    setFilter(fn) {
        if (typeof fn === "function") this.filter = fn;
    }

    static createButton(label, type = ButtonStyle.Primary, fn) {
        let url;
        if (typeof type === "function") {
            fn = type;
            type = ButtonStyle.Primary;
        }
        if (typeof type === "object") {
            url = type.url;
            type = type.type || ButtonStyle.Primary;
        }
        
        let id = Math.random().toString(36).slice(2);

        let button = new ButtonBuilder()
        .setLabel(label)
        .setStyle(type)
        .setCustomId(id);
        if (url) button.setURL(url);

        button.data.fn = fn;

        return button;
    }

    addButton(label, type = ButtonStyle.Primary, fn) {
        let button;
        if (label instanceof ButtonBuilder) {
            button = label;
            if (typeof type === "function") {
                fn = type;
                type = ButtonStyle.Primary;
            }
            if (typeof type === "object") {
                url = type.url;
                type = type.type || ButtonStyle.Primary;
            }
        }
        else button = Components.createButton(label, type, fn);
        fn = fn || button.data.fn;

        this.client.cache.components[button.data.custom_id] = {
            fn,
            created: Date.now()
        };
        
        this.addComponents(button);

        return button;
    }
    
    // addSelectMenu(placeholder, fn) {
    //     let id = Math.random().toString(36).slice(2);

    //     let selectMenu = new MessageSelectMenu();
    //     selectMenu.setPlaceholder(placeholder);
    //     selectMenu.setCustomId(id);
    //     // if (url) selectMenu.setURL(url);

    //     this.client.cache.components[id] = {
    //         fn,
    //         created: Date.now()
    //     };
    //     this.addComponents(selectMenu);
    //     return new ComponentSelectMenu(this.client, selectMenu);
    // }
}

module.exports = Components;