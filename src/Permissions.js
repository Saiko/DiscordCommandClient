module.exports = class {
    constructor(manager) {
        this.manager = manager;
        this._permissions = [];
    }

    add(name, checkFn) {
        let list = typeof name === "object" ? name : { [name.toLowerCase()]: checkFn };
        for (const name in list) {
            const checkFn = list[name];
            if (this._permissions.find(v => v.name === name)) throw new Error();
            this._permissions.push({
                name,
                check: checkFn
            });
        }
    }

    getLevel(name) {
        name = name.toLowerCase();
        return this._permissions.findIndex(v => v.name === name);
    }

    check(member, command) {
        if (this._permissions.length < 1) return true;
        let commandPermission = this.getLevel(command.permissions);
        for (let memberPermission = this._permissions.length - 1; memberPermission > -1; memberPermission--) {
            const permData = this._permissions[memberPermission];

            if (permData?.check(member)) return memberPermission >= commandPermission;
        }
        return false;
    }
};