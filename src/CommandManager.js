const { join, extname, parse, resolve } = require("path");
const Permissions = require("./Permissions");
const Command = require("./Command");
const fs = require("fs-extra");

module.exports = class CommandManager {
    #path = null;

    constructor(commandClient) {
        this.client = commandClient;

        this.permissions = new Permissions(this);

        this.commands = new Map();
        this.removed = new Map();
    }

    /***
     * @param {String|undefined} command Command name or alias
     */
    #getCommand(command) {
        let aliasOrCommand = this.commands.get(command?.toLowerCase());
        if (!aliasOrCommand) return;
        if (aliasOrCommand instanceof Command) return aliasOrCommand;
        return this.commands.get(aliasOrCommand);
    }

    /**
     * 
     * @param {String|Array[String]|Command} args 
     * @returns {Command} 
     */
    get(args) {
        if (args instanceof Command) args = args.callName;      // ???
        if (typeof args === "string") args = args.split(/\s/g);
        if (Array.isArray(args)) return this.#getCommand(args[0])?.subsearch(args.slice(1));
    }

    add(name, prop) {
        name = name.toLowerCase();

        try {
            if (this.commands.has(name)) new Error(`The "${name}" command has already been added`);
            
            prop.name = name;
            let command = new Command(this, prop);
            this.commands.set(name, command);
            this.client.emit("commandAdded", command);
        
        } catch (error) {
            this.client.emit("commandLoadError", error);
        }
    }

    remove(command) {
        command = this.get(command);
        if (!command) return false;

        if (command.callName.length > 1) {
            command.parent.subcommands.delete(command.name);
            command.aliases.forEach(v => command.parent.subcommands.delete(v));
        } else {
            this.commands.delete(command.name);
            command.aliases.forEach(v => this.commands.delete(v));
        }
        return false;
    }

    clear() {
        this.commands.clear();
        // if (removed) this.removed.clear();
    }

    loadOne(path, category = 'other') {
        delete require.cache[require.resolve(path)];
        let prop;
        try {
            prop = require(path);
        } catch(error) {
            this.client.emit("commandLoadError", error);
            return;
        }

        let commandName = parse(path).name.toLowerCase();
        if (this.commands.get(commandName)?.removed) return;
        if (prop.ignore) {
            this.client.emit("commandLoadIgnore", commandName);
            return;
        }

        prop = {
            ...prop,
            path,
            category: category.toLowerCase()
        }

        this.add(commandName, prop);
        return this.get(commandName);
    }

    load(path) {
        this.#path = path;

        let files = [], categorys = [];
        fs.readdirSync(path).forEach(v => 
            fs.statSync(join(path, v)).isDirectory() ?
            categorys.push(v) :
            extname(v) === ".js" ?
                files.push(v) :
                null);
        
        for (const file of files) this.loadOne(join(path, file));
        for (const category of categorys) 
            fs.readdirSync(join(path, category)).forEach(v =>
                extname(v) === ".js" ?
                this.loadOne(join(path, category, v), category) :
                null);
    }

    reload(path = this.#path) {
        if (!path) return;
        this.clear();
        this.load(path);
    }

};
