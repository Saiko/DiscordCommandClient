const { EmbedBuilder, Channel, AttachmentBuilder, Message, TextChannel, ButtonStyle } = require("discord.js");
const Components = require("./Components");
const emoji_regex = require("emoji-regex")();

module.exports = class Context {
    constructor(msg) {
        this.message        = msg;
        this.content        = msg.content;
        this.channel        = msg.channel;
        this.user           = msg.author;
        this.client         = msg.client;
        this.member         = msg.member;
        this.guild          = msg.guild;

        this.componentsRows = [];
        this.pages          = [];
        this.currentPage    = 0;

        this.stringArgs     = this.#splitArgs(this.content);
        
        this.commandArgs    = null;
        this.command        = null;
        this.error          = null;
        this.args           = null;

        this.inited         = false;

    }

    async init() {
        this.inited = true;
        let args = this.#splitArgs(this.content.slice(this.guild.prefix.length));
        this.command = this.getCommand(args);
        if (!this.command) return;
        this.commandArgs = args.slice(this.command.callName.length);
        this.args = await this.parseArgs(this.commandArgs);
    }

    #splitArgs(argsStr) {
        return argsStr.trim().split(/\s+/g)
    }

    async parseArgs(args) {
        let resArgs = [];
        let prefix = "-";
        
        for (let i = 0; i < args.length; i++) {
            let arg = args[i];
            let phrase = null;
            let vPhrase = null;
            if ((new RegExp(`^${prefix}[^\\d]`)).test(arg)) {
                phrase = arg.slice(prefix.length);
                vPhrase = args[i+1];
            }
            
            let string  = this.getString(args.slice(i + !!vPhrase));
            let boolean = this.getBoolean(vPhrase || arg);
            let integer = this.getInteger(vPhrase || arg);
            let channel = await this.getChannel(vPhrase || arg);
            let member  = await this.getMember(vPhrase || arg);
            let emoji   = await this.getEmoji(vPhrase || arg);
            let role    = await this.getRole(vPhrase || arg);
            
            let values = [];
            if (typeof boolean !== 'undefined')    values.push({value:boolean, type:"boolean"});
            if (!isNaN(integer))        values.push({value:integer, type:"integer"});
            if (string)                 values.push({value:string, type:"string"});
            if (channel)                values.push({value:channel, type:"channel"});
            if (channel?.isTextBased())      values.push({value:channel, type:"textchannel"});
            if (channel?.isVoiceBased())     values.push({value:channel, type:"voicechannel"});
            // if (channel?.isDirectroy()) values.push({value:channel, type:"directorychannel"});  
            if (member)                 values.push({value:member, type:"member"}); 
            if (emoji)                  values.push({value:emoji, type:"emoji"});  
            if (role)                   values.push({value:role, type:"role"});   

            resArgs.push({
                phrase,
                values
            });
            if (vPhrase) i++;
            if (string) i += string.split(/\s+/g).length - 1;
        }
        return resArgs;
    }

    
    // Get args
    getIndexLastQuote(args) {
        return args.findIndex(v => /.*[^\\]?\"$/is.test(v));
    }

    getString(args) {
        if (!/^[`'"]/.test(args[0]) || this.getIndexLastQuote(args) < 0) return args[0];
        let res = args.slice(0, this.getIndexLastQuote(args)+1).join(" ");
        return res.substring(1, res.length - 1);
    }

    getInteger(arg) {
        return +arg
    }

    getBoolean(arg) {
        let re = /^(?:1|0|true|false)$/i;
        let re_true = /1|true/i;
        return re.test(arg) ? re_true.test(arg) ? true : false : undefined;
    }
    
    async getEmoji(arg) {
        let re_guildEmoji = /<a?:.+?:(\d+)>/;   
        let re_commonEmoji = emoji_regex;
        let id = re_guildEmoji.exec(arg)?.[1] || ((!isNaN(+arg) && arg > 0) ? arg : undefined);
        if (!id) return;
        return (await this.guild.emojis.fetch(id).catch(_=>undefined)) || re_commonEmoji.test(arg) ? arg : undefined;
    }
    
    async getMember(arg) {
        let re = /^<@!?(\d+)>$/;
        let id = re.exec(arg)?.[1] || ((!isNaN(+arg) && arg > 0) ? arg : undefined);
        if (!id) return;
        return await this.guild.members.fetch(id).catch(_=>undefined);
    }

    async getChannel(arg) {
        let re = /^<#(\d+)>$/;
        let id = re.exec(arg)?.[1] || ((!isNaN(+arg) && arg > 0) ? arg : undefined);
        if (!id) return;
        return await this.guild.channels.fetch(id).catch(_=>undefined);
    }

    async getRole(arg) {
        let re = /^<@&(\d+)>$/;
        let id = re.exec(arg)?.[1] || ((!isNaN(+arg) && arg > 0) ? arg : undefined);
        if (!id) return;
        return await this.guild.roles.fetch(id).catch(_=>undefined);
    }


    create() {
        return new Context(this.message);
    }

    getCommand(args) {
        return this.client.manager.get(args);
    }
    reloadCommand() {
        return this.command = this.command.reload();
    }
    runCommand(args) {
        let command = this.getCommand(args);
        if (!command) throw new Error("Command not found");
        this.command = command;
        return this.command.run(this);
    }
    // exec(command, args) {
    //     if (typeof args !== 'string') throw new Error("Arguments in string type only")
    //     this.args = this.parseArgs(this.#splitArgs(args));
    //     this.runCommand(command);
    // }

    resolveContent(content, payload = {}) {
        let components = [...this.componentsRows];

        payload.reply = this.resolveReply(content.reply);

        if (typeof content === "string") return { ...payload, content, components };
        if (content instanceof Buffer) return { ...payload, files: [{ attachment: content }], components };
        if (content instanceof EmbedBuilder) return { ...payload, embeds: [content], components };        
        if (typeof content === "object") return { ...payload, components, ...content };
    }

    createEmbed(des = {}) {
        let embed = new EmbedBuilder();
        
        let { description, color, url, image, title, footer, author } = des;
        if (description) embed.setDescription(''+description);
        if (color) embed.setColor(color);
        if (title) embed.setTitle(''+title);
        if (url) embed.setURL(url);
        if (footer) {
            if (typeof footer === "object") embed.setFooter(footer);
            else embed.setFooter({ text: ''+footer })
        }
        if (author) {
            if (typeof author === "object") embed.setAuthor(''+author.name, author.icon, author.url);
            else embed.setAuthor(''+author)
        }
        if (image) {
            if (image instanceof Buffer) image = new AttachmentBuilder(image, { name:'unnamed' }).url;
            embed.setImage(image);
        }
        return embed;
    }

    resolveReply(replyObject) {
        if (replyObject instanceof Message) return { messageReference: replyObject };
        else replyObject;
    }

    async send(content, reply) {
        let channel = (content.channel instanceof TextChannel && content.channel) || this.channel;
        let msg = await channel.send(this.resolveContent(content, { reply }));
        this.componentsRows = [];
        return msg;
    }
    async edit(msg, content, reply) {
        msg = await msg.edit(this.resolveContent(content, { reply }));
        // this.componentsRows = []; // ???
        return msg;
    }

    async sendEmbed(des, reply) {
        if (typeof des === "string") return await this.send({ embeds: [new EmbedBuilder().setDescription(des)], reply });
        return await this.send({
            content: des.content,
            channel: des.channel,
            embeds: [this.createEmbed(des)],
            reply 
        });
    }
    async editEmbed(msg, des, reply) {
        if (typeof des === "string") return await this.edit(msg, { embeds: [new EmbedBuilder().setDescription(des)], reply });
        return await this.edit(msg, {
            content: des.content,
            embeds: [this.createEmbed(des)],
            reply
        });
    }

    // Interactions
    createRow() {
        return new Components(this.client);
    }
    addRow() {
        let component = this.createRow();
        this.componentsRows.push(component);
        return component; 
    }
    getComponentsRows() {
        return this.componentsRows;
    }

    createButton(label = "Button", type, fn) {
        return Components.createButton(label, type, fn);
    }
    addButton(label = "Button", type, fn) {
        if (this.componentsRows.length < 1 ||
            this.componentsRows[this.componentsRows.length - 1].length === 5)
            this.addRow();
        return this.componentsRows[this.componentsRows.length - 1].addButton(label, type, fn);
    }

    // Pagenator
    addPage(content) {
        if (!content) throw new Error("Page content is not specified");
        content = this.resolveContent(content);
        content.components = undefined;
        this.pages.push(content);
    }

    sendPage(index = 0) {
        if (typeof index !== "number") throw new Error("The index must be a number");
        if (index < 0 || index >= this.pages.length) throw new Error("The index is out of range");
        this.currentPage = index;
        
        this.addButton("<", i => {
            this.currentPage--;
            if (this.currentPage < 0) this.currentPage = this.pages.length - 1;
            i.update({...this.pages[this.currentPage]});
        });
        this.addButton("X", ButtonStyle.Danger, i => {
            i.message.delete();
        })
        this.addButton(">", i => {
            this.currentPage++;
            if (this.currentPage > this.pages.length - 1) this.currentPage = 0;
            i.update({...this.pages[this.currentPage]});
        });
        
        this.send({...this.pages[this.currentPage], components: this.getComponentsRows()});
    }


    async awaitMessages(channel, options) {
        if (!(channel instanceof Channel)) {
            options = channel;
            channel = this.channel;
        }
        if (!channel.isText()) throw new Error("The channel is not a TextChannel");
        return new Promise((resolve, reject) => {
            const collector = channel.createMessageCollector(options);
            collector.once('end', (collection, reason) => {
                if (options.errors?.includes(reason)) {
                    reject(collection, reason);
                } else {
                    resolve(collection);
                }
            });
        });
    }

    // async sendEmbedQuestion(embedOrDescription, options = {}) {
    //     return new Promise(async (res, rej) => {
    //         let botMsg = await this.sendEmbed(embedOrDescription).catch(rej);
    //         this.awaitMessages(this.channel, { max: 1, time: 60_000, ...options }).then(v => {
    //             let userMsg = v.first()
    //             if (options.delete && !msg.deleted) msg.delete();
    //             res([botMsg, userMsg]);
    //         }).catch(rej);
    //     });
    // }
    // async editEmbedQuestion(msg, embedOrDescription, options = {}) {
    //     await this.editEmbed(msg, embedOrDescription);
    //     return await this.awaitMessages(msg.channel, { max: 1, time: 60_000, ...options }).then(v => (options.delete && !msg.deleted ? msg.delete() : null, v.first()));
    // }
}